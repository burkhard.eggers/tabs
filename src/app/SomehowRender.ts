import { ChangeDetectorRef, Component, ComponentFactoryResolver, Input, OnInit, ViewContainerRef } from "@angular/core";

@Component({
  selector: 'ng-somehow-render',
  template: '',
})
export class NgSomehowRenderComponent implements OnInit {
  @Input() instance: any;

  constructor(
    private cfr: ComponentFactoryResolver,
    private vcr: ViewContainerRef,
    private cdr: ChangeDetectorRef,
  ) {
  }

  public ngOnInit(): void {
    const componentFactory = this.cfr.resolveComponentFactory(this.instance.constructor);

    this.vcr.clear();
    const componentRef = this.vcr.createComponent(componentFactory);
    for (const key of Object.getOwnPropertyNames(this.instance)) {
      componentRef.instance[key] = this.instance[key];
    }
    this.cdr.detectChanges();
  }
}