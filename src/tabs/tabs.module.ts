import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsComponent } from './tabs/tabs.component';
import { TabComponent } from './tab.component';
import { Title } from './tabs-title.component';


@NgModule({
  declarations: [TabsComponent, TabComponent, Title],
  imports: [
    CommonModule
  ],
  exports: [
    TabsComponent,
    TabComponent,
    Title
  ]
})
export class TabsModule { }
