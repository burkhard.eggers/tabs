import { AfterContentInit, Component, ContentChildren, OnInit, ViewChildren } from '@angular/core';
import { QueryList } from '@angular/core';
import { TabComponent } from '../tab.component';

@Component({
  selector: 'tabs-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.sass']
})
export class TabsComponent implements AfterContentInit {
  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

  constructor() { }
  activate(tab: TabComponent): void {
    this.tabs.forEach(tab => tab.active = false);
    tab.active = true;
  }

  ngAfterContentInit() {
    console.log('tabs', this.tabs);
    const toSelect: TabComponent =
      this.tabs.toArray().find(tab => tab.initiallySelected) || this.tabs.first;
    toSelect.active = true;
  }
}
