import { Component, Input, ContentChild } from '@angular/core';
import { Title } from './tabs-title.component';

@Component({
  selector: 'tabs-tab',
  template: `
  <div *ngIf="active">
    <ng-content></ng-content>
  </div>
  `
})
export class TabComponent {

  @Input() initiallySelected: boolean;
  @ContentChild(Title) title: Title;
  @Input('title') titleInput: string;
  constructor() { }
  active: boolean = false;

}
