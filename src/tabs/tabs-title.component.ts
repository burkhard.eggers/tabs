import { Directive, TemplateRef } from "@angular/core";

@Directive({
  selector: '[tabs-title]'
})
export class Title {
  constructor(public template: TemplateRef<any>) { }
}