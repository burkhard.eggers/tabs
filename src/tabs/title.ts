import { Component, Input, ContentChild } from '@angular/core';
import { Title } from './tabs-title.component';

@Component({
  selector: 'tabs-title',
  template: `
  <div>
    <ng-content></ng-content>
  </div>
  `
})
export class TitleComponent {
}
